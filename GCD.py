class gcd(object):

    def __init__(self, a, b):
        """
        Class to calculate the Greatest Common Divisor of a and b
        along with integers x and y so that a*x + b*y = 1

        """
        self.a = a
        self.b = b
        self.results = [{self.a: [1,0] }, {self.b: [0,1] }]
        self.gcd_result = ''

    def __gcd(self, a, b):
        """
        recrusive function to calculate the gcd
        """
        if b == 0: 
            return a if a > 0 else a * -1

        r = a % b 
        q = a // b

        print(f"{a} = ({q})*{b} + {r}")

        if r != 0:
            self.results.append({r: []})

            return self.__gcd(b,r)

        self.gcd_result = b
        return b if b > 0 else b * -1 
        
    def calc_gcd(self):
        return self.__gcd(self.a, self.b)
    
    def print_gcd(self):
        print(f"gcd result is:{self.gcd_result}")
    
    def __calc_table_val(self, i):
        row_pos_1 = self.results[i-1]
        row_pos_2 = self.results[i-2]

        items1 = list(row_pos_1.items())[0]
        items2 = list(row_pos_2.items())[0]

        q = items2[0] // items1[0] 

        x = items2[1][0] - (q * items1[1][0])
        y = items2[1][1] - (q * items1[1][1])
        return [x, y]
    
    def calc_integers(self):
        # starting lenght of results when init called is 2 
        if len(self.results) == 2:
            _ = self.__gcd(self.a, self.b)
        
        for row in self.results:
            items = list(row.items())[0]
            b, table_values = items[0], items[1]

            if len(table_values) == 0:
                i = self.results.index(row)
                row[b] = self.__calc_table_val(i)

        last = list(self.results[-1].items())[0]
        return last[1]
    
    def __fill_table_results(self):
        if len(self.results) == 2:
            _ = self.calc_integers()
    
    def calc_other_integers_x_and_y(self):
        """
        a different pair of x and y so that x*a + y*b is also 1
        """
        self.__fill_table_results()

        last = list(self.results[-1].values())[0]
        x = self.b + last[0]
        y = last[1] - self.a
        return [x, y]

    
    def display_table_results(self):
        # starting lenght of results when init called is 2 
        print("\nFinding the gcd:\n")
        self.__fill_table_results()
        
        
        print("\nTable Results:\n")
        for row in self.results:
            print(row)

        print("\nEquation:\n")
        last_values = list(self.results[-1].values())[0]
        print(f"{self.a} * {last_values[0]} + {self.b} * {last_values[1]} = 1")


if __name__ == "__main__":

    ### example ### 
    a = 96
    b = 89
    gcd_example = gcd(a, b)
    gcd_example.display_table_results()

    """
    The following will print out: 

    Finding the gcd:

    96 = (1)*89 + 7
    89 = (12)*7 + 5
    7 = (1)*5 + 2
    5 = (2)*2 + 1
    2 = (2)*1 + 0

    Table Results:

    {96: [1, 0]}
    {89: [0, 1]}
    {7: [1, -1]}
    {5: [-12, 13]}
    {2: [13, -14]}
    {1: [-38, 41]}

    Equation:

    96 * -38 + 89 * 41 = 1

    """
    

    
