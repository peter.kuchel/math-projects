from itertools import chain, combinations, product


class SetTools(object):
    
    """
    class to help with dealing with sets 
    """
    
    def __power_set(self, iterable):
        elements = list(iterable)
        ### itertools provides this method in their documentation which is very helpful ### 
        power_set = chain.from_iterable(combinations(elements, r) for r in range(len(elements)+1))
        return list(power_set) 
    
    def power_set(self, A):
        items = self.__power_set(A)
        return [set(i) for i in items]
    
    def power_set_cartesian_product(self, A, B):
        all_items = self.cartesian_product(A, B)
        power_set_items = self.__power_set(all_items)
        return power_set_items

    def cartesian_product(self, A, B):
        return list(product(A, B))

class Functions(object):

    """
    class to return one-to-one and onto functions, along with all mappings  
    """

    def __init__(self, domain, codomain):
        """
        legend:
            - domain : A
            - co-domain : B

        """
        if not (isinstance(domain, set) and isinstance(codomain, set)):
            raise Exception("Domain and Co-domain must be of type set")
        self.domain = domain
        self.codomain = codomain

        self.domain_size = len(self.domain)
        self.codomain_size = len(self.codomain)
        self.set_tool = SetTools()
        
    def all_mappings(self):
        return self.set_tool.cartesian_product(self.domain, self.codomain)
    
    def all_one_to_one_functions(self):
        """
        a function f is one-to-one (injective) iff for all a,b in A, if f(a) = f(b), then a = b
        """
        funcs = []
        if self.domain_size > self.codomain_size:
            return funcs

        function_mappings = self.all_mappings()
        all_possible = chain.from_iterable(combinations(function_mappings, r) for r in range(1, self.domain_size+1))
        all_possible = list(all_possible)

        
        fit_to_cardinality = [i for i in all_possible if len(i) == self.domain_size]
        for f in fit_to_cardinality:
            domain_elements = {i[0] for i in f}
            codomain_elements = {j[1] for j in f}
            if (len(domain_elements) == self.domain_size) and (len(codomain_elements) == self.domain_size):
                funcs.append(f)

        return funcs 
    
    def all_onto_functions(self):   
        """
        a function g is onto (surjective) iff for all y in B, there exists x in A so that f(x) = y 
        """
        funcs = []
        size = self.codomain_size
        if size > self.domain_size:
            return funcs
        
        if (self.codomain_size < self.domain_size):
            size = self.domain_size

        function_mappings = self.all_mappings()
        all_possible = chain.from_iterable(combinations(function_mappings, r) for r in range(1, size+1))

        all_possible = list(all_possible)
        fit_to_cardinality = [i for i in all_possible if len(i) == size]
        
        for f in fit_to_cardinality:
            domain_elements = {i[0] for i in f}
            codomain_elements = {j[1] for j in f}
            if (len(domain_elements) == self.domain_size) and (len(codomain_elements) == self.codomain_size):
                funcs.append(f)

        return funcs 


if __name__ == '__main__':

    ### example use: ###

    # non-empty sets 
    A = {1, 2}
    B = {4, 5, 6}

    # create class 
    F = Functions(A,B)

    # get all ono-to-one functions
    f = F.all_one_to_one_functions()
    print(f)
    """
    prints: 

    [((1, 4), (2, 5)), 
    ((1, 4), (2, 6)), 
    ((1, 5), (2, 4)), 
    ((1, 5), (2, 6)), 
    ((1, 6), (2, 4)), 
    ((1, 6), (2, 5))]
    """
    # get all onto functions 
    g = F.all_onto_functions()
    print(g)
    """
    prints:
    [] 
    (there does not exist an onto function in A x B)
    """

    